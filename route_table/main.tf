resource "aws_route_table" "CloudAppi-RT" {
  vpc_id = var.vpc_input
 route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.ig_input
  }
  tags = {
    Name = "CloudAppi-Route"
    Owner = "Leonel Ferrari"
  }
}