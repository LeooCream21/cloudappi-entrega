output "vpc_id_output" {
   value =  aws_vpc.Code-Challenge-CloudAppi.id
}

output "cidr_block_output" {
   value =  aws_vpc.Code-Challenge-CloudAppi.cidr_block
}
