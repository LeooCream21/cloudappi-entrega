#Crea la VPC 
resource "aws_vpc" "Code-Challenge-CloudAppi" {

  cidr_block = var.cidr_block_vpc
  enable_dns_hostnames = true
  tags = {
    "Name" = "Code-Challenge-CloudAppi"
    "Owner" = " Leonel Ferrari"
  }
}