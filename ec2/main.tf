
resource "aws_instance" "Swagger-Api" {
  
  ami = var.imagen_ami
  instance_type = "t2.micro"
  subnet_id  = var.id_subnet_input #llamar aca otro modulo
  key_name = var.name_key_input #llamar algun modulo
  vpc_security_group_ids  = [var.security_group_input]  #llamar algun modulo
  associate_public_ip_address = "true"
  user_data = var.unScript
  private_ip = "10.100.10.99"

  tags = {
    Name = "CloudAppi-Swagger"
    "Demo/Cliente" = "CloudAppi"
    tipo = "Managing with Terraform v 0.12.24"
    Owner = "Leo"
  }
}