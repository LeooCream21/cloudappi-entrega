terraform {
  required_version = ">= 0.12.24"
}

provider "aws"{
  shared_credentials_file = "/home/lferrari/.aws/credentials"
  region  = var.region_aws
  profile = var.profile_provider
  version = "~> 2.0"

}

module "vpc" { 
  source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/vpc"  
  cidr_block_vpc = var.range_ip
}

module "ig" {
    source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/ig"
    vpc_id_input = module.vpc.vpc_id_output

}

module "rt" {
  source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/route_table"
  vpc_input = module.vpc.vpc_id_output
  ig_input = "igw-065147d331439ed02"
}

module "elb" {
  source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/elb"
}

module "subnet" {
    source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/subnet"
    vpc_id_input = module.vpc.vpc_id_output
    cidr_block_subnet = var.block_subnet
}

module "sg" {
    source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/sg"
    vpc_id_input = module.vpc.vpc_id_output
    vpc_cidr_bloks_input = [module.vpc.cidr_block_output]
}

module "key_pair" {
    source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/key_pair"
    key_name_input = var.key_name_ec2
    public_key_path = var.public_key_path
}

module "ec2" {

   source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/ec2"
   id_subnet_input =  module.subnet.subnet_id_output
   imagen_ami = var.imagen_ami
   security_group_input = var.sg_hard
   name_key_input = module.key_pair.name_key_output
   unScript = "${file("script.sh")}"
}

module "eip" {
  source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/eip"
  instancia_id = module.ec2.ec2_id_output

}

module "launch_conf" {
  source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/launch_configuration"
  imagen_ami = var.imagen_ami
}

module "asg" {
  source = "/home/lferrari/Escritorio/repos-git/cloudappi-entrega/asg"
  agente_launch_conf = module.launch_conf.agent_launch_configuration_output
}
