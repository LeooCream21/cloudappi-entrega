resource "aws_internet_gateway" "CloudAppi-IG" {
  #aws_vpc.usando-terraform-vpc-elk.id
  vpc_id = var.vpc_id_input
  tags = {
    "Name" = "IG-CloudAppi"
    "Owner" = "Leonel Ferrari"
  }
}