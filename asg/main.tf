resource "aws_autoscaling_group" "agentes" {
    availability_zones = ["us-east-1f"]
    name = "agentes_asg"
    max_size = "2"
    min_size = "1"
    health_check_grace_period = 300
    health_check_type = "EC2"
    desired_capacity = 1
    force_delete = true
    launch_configuration = var.agente_launch_conf

    tag {
        key = "Name"
        value = "Agent Instance"
        propagate_at_launch = true
    }
}