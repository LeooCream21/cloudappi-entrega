##PROVIDER##
variable "profile_provider" {  
    type = string
    description = "Provider"
}

##REGION##
variable "region_aws" {  
    type = string
    description = "Region"
}

##Virtual Private Network##
variable "range_ip" {  
    type = string
    description = "Cidr_block"
}

##Subnet##
variable "block_subnet" {  
    type = string
    description = "Block subnet"
}

##Imagen##
variable "imagen_ami" {  
    type = string
    description = ""
}

##Security Group##
variable "sg_hard" {  
    type = string
    description = ""
}

##EC2##
variable "key_name_ec2" {  
    type = string
    description = ""
}

##Llaves##
variable "public_key_path" {  
    type = string
    description = ""
}