
resource "aws_launch_configuration" "agent_launch_conf" {
    name_prefix = "agent-lc-"
    image_id = var.imagen_ami
    instance_type = "t2.micro"
    user_data = "${file("iniciar-agente-ins.sh")}"

    lifecycle {
        create_before_destroy = true
    }

    root_block_device {
        volume_type = "gp2"
        volume_size = "8"
    }
}