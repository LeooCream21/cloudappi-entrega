#! /bin/bash
sudo su -
sudo add-apt-repository -y -r ppa:chris-lea/node.js
sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list
sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list.save
curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
echo "deb https://deb.nodesource.com/node_12.x bionic main" | sudo tee /etc/apt/sources.list.d/nodejssource.list
sudo apt-get update
sudo apt-get install nodejs -y

#Instalar NGINX
sudo apt install nginx -y
sudo apt update

#"
npm install -g http-server
git clone https://github.com/swagger-api/swagger-editor.git
http-server -p 8080 swagger-editor