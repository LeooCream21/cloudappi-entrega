resource "aws_key_pair" "key_pair" {
  key_name = var.key_name_input
  public_key = "${file(var.public_key_path)}"
}