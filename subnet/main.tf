#Crea una subnet ()
resource "aws_subnet" "Code-Challenge-CloudAppi-Subnet" {
  vpc_id = var.vpc_id_input
  cidr_block = var.cidr_block_subnet
  availability_zone = "us-east-1a"

  tags = {
    "Name" = "Code-Challenge-CloudAppi-Subnet"
    "Owner" = "Leonel Ferrari"
  }
}