resource "aws_eip" "ip-instancia-ec2" { 
  
  vpc = true
  instance = var.instancia_id
  associate_with_private_ip = "10.100.10.99"
  #depends_on = var.ig_input
  tags = {
    description = "EIP "
    name = "Elastic IP for the instance"
  }
}